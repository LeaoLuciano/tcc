%!TeX root=../tese.tex
%("dica" para o editor de texto: este arquivo é parte de um documento maior)
% para saber mais: https://tex.stackexchange.com/q/78101

% Vamos definir alguns comandos auxiliares para facilitar.

% "textbackslash" é muito comprido.
\newcommand{\sla}{\textbackslash}

% Vamos escrever comandos (como "make" ou "itemize") com formatação especial.
\newcommand{\cmd}[1]{\textsf{#1}}

% Idem para packages; aqui estamos usando a mesma formatação de \cmd,
% mas poderíamos escolher outra.
\newcommand{\pkg}[1]{\textsf{#1}}

% A maioria dos comandos LaTeX começa com "\"; vamos criar um
% comando que já coloca essa barra e formata com "\cmd".
\newcommand{\ltxcmd}[1]{\cmd{\sla{}#1}}

\chapter{Trapaças em jogos digitais}
\label{chap:trapacas}

%\textit{Revisar: \citep{gautheierdickey2004low}}

Nesse capítulo, serão analisadas as possíveis formas de trapacear em 
jogos digitais em rede.


\section{Definição}
Para elaborar uma classificação das possíveis trapaças, primeiramente é necessário definir 
o que é trapacear em jogos (digitais ou não). \citet{yan2002security}
 define trapaça como qualquer \textit{"Atitude que um jogador possa tomar para
adquirir uma vantagem injusta, ou alcançar um estado que não deveria ser alcançável"} (tradução livre).
Já \citet{yan2009investigation} reformula a definição anterior: 


\textit{
    Qualquer atitude que o jogador use para adquirir vantagem sobre os demais jogadores
    ou alcançar um estado é trapaça se, de acordo com as regras do jogo ou a critério
    do operador do jogo (isso é, o provedor do serviço do jogo que não necessariamente é
    o desenvolvedor do jogo), a vantagem ou o estado é tal que o jogador não deveria 
    ter alcançado
} (tradução livre).

Note que não necessariamente todas as restrições impostas pelas regras do jogo estão implementadas
diretamente no \textit{\textit{software}}, em certas ocasiões nem é possível implementá-las. Por exemplo, não
há como um \textit{software} impedir que um jogador registre num papel um conteúdo no qual as regras do
jogo estabeleciam que o jogador memorizasse.

A partir dessa definição, também é possível inferir que o operador do jogo pode determinar se certo 
comportamento não é permitido mesmo que não seja explicitamente 
proibido pelas regras do jogo. Logo, ações que não foram levadas 
em consideração durante a elaboração das regras podem ser julgados como 
trapaças pelo operador durante ou posteriormente à execução do jogo.

Essa última definição será a adotada por essa monografia. A partir disso, já é possível
categorizar as trapaças em jogos digitais.



\section{Estrutura das classificações}

A classificação das trapaças nesse texto é baseada na divisão elaborada por 
\citet{webb2007racs} que separa as trapaças em quatro níveis (ilustrados na Figura \ref{fig:niveis}):
jogo (Seção \ref{sec:jogo}), aplicação (Seção \ref{sec:aplicacao}),
 protocolo (Seção \ref{sec:protocolo}) e infraestrutura (Seção \ref{sec:infraestrutura}).
As trapaças que envolvem falhas de segurança em geral não foram levadas
 em consideração (com exceção de \textit{Spoofing}).

Além disso, as seções a seguir exibem exemplos 
de cada tipo de trapaça. Os exemplos seguem a seguinte notação:
% \begin{itemize}
%     \item[$M$:] representa o jogador que comete a trapaça;
%     \item[$A$, $B$ e $C$:] representam jogadores honestos;
%     \item[$X$ e $Y$:] representam avatares de jogadores.
% \end{itemize}
\begin{itemize}[label={}]
    \item$M$: representa o jogador que comete a trapaça;
    \item$A$, $B$ e $C$: representam jogadores honestos;
    \item$X$ e $Y$: representam avatares de jogadores.
\end{itemize}
% \begin{tabular}{ll}
%     M: & o jogador que comete a trapaça; \\
%     A, B e  C: & jogadores honestos; \\
%     X e Y: & avatares de jogadores honestos ou não.
% \end{tabular}



\begin{figure}[h]
    \includegraphics[width=0.8\textwidth]{figuras/niveis.eps}
\caption{Um diagrama ilustrando os níveis de trapaças}
\label{fig:niveis}
\end{figure}

\section{Nível de jogo}
\label{sec:jogo}
Nesse primeiro nível, usuários mal-intencionados conseguem trapacear utilizando somente a 
interface do jogo, sem necessitar modificar qualquer \textit{software} ou hardware.
Portanto, é possível que jogadores sem conhecimento técnico ou sem más intenções 
consigam trapacear. 

Há dois tipos de trapaça nesse grupo: por meio de falha de implementação
e de falha de design de jogo.




\subsection{Falha de implementação}

Descuidos dos programadores no código fonte do jogo podem gerar vulnerabilidades,
das quais os jogadores
podem tirar proveito sem que necessitem modificar o jogo ou utilizar algum outro \textit{software}.


%Jogadores mal-intencionados podem se aproveitar de falhas na implementação sem necessitar de meios externos.


Exemplo 1:
\begin{itemize}[label={}]
%    \item Gênero: RPG\footnote{\textit{Role-playing game} }
    \item Contexto: Os programadores não trataram o caso de \textit{underflow} de
    inteiros no cálculo dos pontos de vida após um ataque.
    \item Trapaça: Sob um efeito que diminui sua força de ataque, $M$ obtém uma força de ataque negativa e retira uma quantidade absurda de pontos de vida de $A$ ao atacá-lo.
\end{itemize}

Exemplo 2:
\begin{itemize}[label={}]
    \item Gênero: Simulação 3D.
    \item Contexto: Colisões só são detectadas quando as caixas de colisão se sobrepõem.
    \item Trapaça: $M$ atravessa paredes ao se movimentar muito rápido, pois 
    sua caixa de colisão não sobrepõe com a da parede em nenhum dos frames que ocorrem durante a movimentação.
\end{itemize}

\subsection{Falha de design de jogo}

%\textit{[inserir definição de game design]}.
Trapaças através de falha no design ocorrem em situações nas
quais os jogadores, seguindo as regras do jogo, alcançam condições vantajosas
não previstas pelo designers
de jogo e que são consideradas injustas.


Exemplo 1:
\begin{itemize}[label={}]
    \item Gênero: RPG \footnote{\textit{Role-playing game}, gênero de jogo no qual os jogadores controlam personagens e avançam numa narrativa. Os personagens tipicamente possuem características e equipamentos que os permitem um melhor desempenho.}.
    \item Contexto: 
     No mundo do jogo é possível obter um escudo que reduz o dano recebido pela metade e uma armadura que replica o efeito do escudo equipado
    % Equipamentos geram benefícios ao usuário.
    % \item M se torna invulnerável ao equipar um capacete que torna 
    % o usuário imune a ataques durante 2s após perder pontos de vida, 
    % um colar que causa 1 ponto de dano por segundo e um anel que cura 
    % 2 pontos de vida por segundo. 
    \item Trapaça: $M$ equipa o escudo e a armadura, tornando-o invulnerável.
\end{itemize}

Exemplo 2:
\begin{itemize}[label={}]
    \item Gênero: Jogo de cartas.
    \item Contexto: Os desenvolvedores não limitaram a duração de tempo dos turnos.
    \item Trapaça: $M$ não finaliza seu turno para que A desista da partida e M vença por desistência. 
\end{itemize}


\section{Nível de aplicação}
\label{sec:aplicacao}

A nível de aplicação, os jogadores mal-intencionados trapaceiam através
de modificações nos arquivos executáveis e de dados, ou utilizando \textit{software} para acessar
ou alterar a memória reservada ao jogo. 

Observe que, em geral, é necessário conhecimento 
em programação, técnicas de engenharia reversa e formatos de arquivos. Porém, é possível
que mesmo jogadores leigos nesses assuntos efetuem trapaças desse tipo, através de 
\textit{software} de terceiros específicos para certo jogo ou não, que possuem uma interface 
de usuário amigável que permite que o usuário altere o jogo facilmente. Vale ressaltar que
jogos de código aberto permitem alterações em seus \textit{software} de forma relativamente simples,
tornando-os mais suscetíveis a esse nível de trapaça.

Nesse nível há três tipos de trapaça: por meio de exposição de informação, de facilitadores e
de comandos inválidos.


\subsection{Exposição de informação}

A trapaça por exposição de informação acontece no momento no qual o 
jogador consegue acessar conteúdo 
secreto do estado do jogo, isso é, que está armazenado em sua máquina mas não é transmitido 
ao usuário.


Exemplo 1:
\begin{itemize}[label={}]
    \item Gênero: Jogo de Cartas.
    \item Trapaça: $M$ acessa a memória para visualizar as variáveis que contém os valores das cartas dos adversários.
\end{itemize}

Exemplo 2:
\begin{itemize}[label={}]
    \item Gênero: FPS \footnote{\textit{First-person shooter}, gênero de jogo em primeira pessoa focado no combate com armas de fogo. }.
    \item Trapaça: $M$ altera o código fonte do jogo para enxergar no escuro e através de paredes.
\end{itemize}

\subsection{Facilitadores}

Facilitadores são modificações no jogo ou \textit{software} externo que criam comandos de entradas no jogo,
e possuem o objetivo de auxiliar o jogador, muitas vezes utilizados a fim de executar tarefas repetitivas
ou que exijam reflexos apurados.


Exemplo 1:
\begin{itemize}[label={}]
    \item Gênero: FPS.
    \item Trapaça: $M$ insere um algoritmo no código do jogo para mirar automaticamente no alvo.
\end{itemize}

Exemplo 2:
\begin{itemize}[label={}]
    \item Gênero: RPG.
    \item Contexto: Equipar um escudo previne ataques de inimigos.
    \item Trapaça: $M$ insere um algoritmo no código fonte que detecta ataques inimigos e equipa o escudo automaticamente.
\end{itemize}

\subsection{Comando inválido}

% Em jogos em rede, o estado global do jogo é modificado através de comandos 
% chamados pelo jogador e é compartilhado aos demais nós através de eventos ou 
% de atualizações, conforme visto no Capítulo \ref{cap:conceitos}. 

Em jogos digitais, comandos são eventos que alteram o estado do jogo. 
A trapaça por comando inválido ocorre quando o jogador modifica o jogo para inserir ou modificar comandos que o tragam vantagem. Em jogos em rede, os comandos inválidos podem ser transmitidos aos outros nós, mas a depender do protocolo, é possível que esses comandos não sejam aceitos ou reconhecidos pelos outros nós, tornando o estado local de jogo do usuário mal-intencionado inconsistente com o estado global.

Em modelos de rede Cliente/Servidor ou modelos híbridos, essa trapaça pode ser detectada e 
corrigida mais facilmente, pois o nó servidor pode validar os comandos e detectar quem enviou os comandos alterados.
Já no modelo \textit{Peer-to-Peer}, há jogos que os comandos só são enviados para os nós jogadores numa área de interesse, então o jogador pode usar comandos inválidos sem ser detectado quando não há ninguém por perto. Além disso, mesmo se um nó conseguir identificar que outro nó enviou comandos inválidos e alertar os outros nós sobre isso, os outros nós não podem garantir que o nó que alertou não está mentindo.  

% Já que em arquitetura sem nós confiáveis é necessário
% que o nós validem os comandos dos outros e entrem em consenso, veja
% o Capítulo \ref{cap:abordagens} para uma discussão mais elaborada.





Exemplo 1:
\begin{itemize}[label={}]
    \item Gênero: Jogo de cartas.
    \item Trapaça: $M$ modifica a memória para aumentar o valor da variável que 
    corresponde ao limite de cartas na mão para que consiga usar mais vezes o comando de obter mais cartas.
\end{itemize}

Exemplo 2:
\begin{itemize}[label={}]
    \item Gênero: Simulação.
    \item Trapaça: $M$ adiciona uma comando que, ao pressionar um botão, o permite teleportar ao local que desejar.
\end{itemize}



\section{Nível de protocolo}
\label{sec:protocolo}

Trapaças a nível de protocolo envolvem manipulação direta nos dados 
transmitidos entre os nós, ou seja, podem alterar, adicionar, remover
ou duplicar pacotes (\citet{webb2008survey}). Portanto, para executar essa trapaça,
é essencial conhecimento em protocolos de comunicação e também pode ser necessário
entendimentos de criptografia.
Contudo, um jogador leigo pode utilizar \textit{software} de terceiros específicos para o protocolo
usado no jogo para interferir nos pacotes.

Nesse nível há nove tipos de trapaça: por supressão de atualização, atraso fixo,
remarcação de tempo, inconsistência, complô, \textit{spoofing}, \textit{replay}, oponentes cegos
e revogação.


\subsection{Supressão de atualização}
% Em jogos distribuídos com modelo de consistência orientado a atualizações e que permitem
% perda de pacotes de atualizações (normalmente jogos em tempo real), em geral adotam uma 
% estratégia de desconectar o jogador após $n$ pacotes perdidos seguidos. Dessa maneira, 
% a trapaça por supressão de atualização consiste no jogador omitir $n-1$ pacotes e enviar 
% $n$-ésimo, assim ocultando o estado do se

% Supressão de atualização

Em jogos que utilizam a técnica de \textit{dead-reckoning}, em geral, adotam uma 
estratégia de desconectar o jogador após um certo número $n$ de atualizações perdidos em seguidas. Dessa maneira, 
a trapaça por supressão de atualização consiste no jogador omitir $n-1$ atualizações aos outros nós e enviar somente a 
$n$-ésima, assim ocultando o estado real do seu avatar para os outros jogadores.

Exemplo:
\begin{itemize}[label={}]
    \item Gênero: FPS.
    \item Trapaça: $M$ se movimenta no mundo sem informar sua posição frequentemente aos outros jogadores,
     deixando de enviar $n-1$ atualizações, mas transmitindo a $n$-ésima para não ser 
     desconectado por inatividade. Na perspectiva dos outros jogadores, M estará
     se teleportando pelo mundo, tornando-o um alvo mais difícil de acertar.
\end{itemize}

\subsection{Atraso Fixo}
A Trapaça por Atraso Fixo acontece quando o jogador mal-intencionado 
atrasa de propósito o envio de pacotes a fim de fazer a jogada ótima 
ou de diminuir o tempo para adversários os reagirem.

Exemplo:
\begin{itemize}[label={}]
    \item Gênero: Jogo de perguntas e respostas.
    \item Trapaça: $M$ espera $A$, $B$ e $C$ divulgarem suas respostas para escolher a resposta pela qual a maioria optou. 
\end{itemize}


\subsection{Remarcação de tempo}
Em jogos que usam o modelo de consistência otimista é comum que atualizações transmitidas entre os nós tenham informações do instante de tempo
no qual foram enviadas. Um jogador pode alterar essa marcação de tempo para alterar o passado do estado do jogo e assim obter vantagem.


Exemp:
\begin{itemize}[label={}]
    \item Gênero: Jogo de Luta.
    \item Contexto: Os jogadores que aplicam um golpe ficam vulneráveis durante os primeiros milissegundos do golpe.
    \item Trapaça: $M$ aplica um golpe em $A$, mas mente ao gerar o pacote da mensagem, 
    informando que o golpe se iniciou milissegundos antes do tempo atual, pulando a parte vulnerável do golpe e não deixando oportunidade para $A$ reagir.
\end{itemize}

% \textit{(Será que informar um mensagem do futuro pode ser vantajoso?)}

\subsection{Inconsistência}

Trapaça por inconsistência ocorre quando um nó envia atualizações diferentes 
para um conjunto de nós, gerando estados globais de jogo discordantes entre os nós.
Note que, essa trapaça, geralmente, não acontece em arquiteturas C/S, pois o servidor impõe seu estado de jogo aos clientes.


Exemplo:
\begin{itemize}[label={}]
    \item Contexto: Os jogadores realizam uma votação.
    \item Trapaça: $M$ transmite para $A$ que votou na opção 1 e para $B$ que votou em 2, 
    tornando os estados do jogo de $A$ e $B$ contraditórios, estragando a experiência de jogo.
\end{itemize}


\subsection{Complô}

% \textit{alguns artigos parecem discordar um pouco da definição}

Complô ocorre quando jogadores (ou somente um jogador 
controlando múltiplas instâncias do jogo) de equipes adversárias 
cooperam entre si, geralmente utilizando meios de comunicação externos.

Note que não necessariamente todos os jogadores envolvidos na trapaça
ficarão com vantagem, pelo contrário, é possível que alguns se prejudiquem
para favorecer outros.

Exemplo:
\begin{itemize}[label={}]
    \item Gênero: jogo de futebol.
    \item Contexto: $M$ joga pelo time 1 mas também controla $X$ do time 2.
    \item Trapaça: $X$ realiza gols contra com o intuito de ajudar a equipe 1 a vencer.
\end{itemize}



\subsection{\textit{Spoofing}}
No contexto de segurança de redes, ataque de Spoofing acontece
 quando um transmissor legítimo para de se comunicar com um receptor 
 legítimo e o atacante inicia envio de mensagens ao receptor legítimo, 
 fingindo ser o transmissor legítimo
  \citep{yilmaz2015survey}.
Portanto, de forma semelhante ao ataque, a trapaça de Spoofing ocorre
quando um nó se passa por outro(s) nó(s) com intuito de obter 
informações secretas ou de modificar o estado do jogo ao seu favor.

Exemplo:
\begin{itemize}[label={}]
    \item Gênero: RPG.
    \item Contexto: $M$ controla $X$ e $A$ controla $Y$.
    \item Trapaça: $M$ se identifica como $Y$ aos demais nós com intuito de fornecer os itens de $Y$ para $X$ sem o consentimento de $A$.
\end{itemize}


\subsection{\textit{Replay}}
A trapaça por Replay ocorre quando cada jogador transmite suas jogadas 
de forma criptografada (geralmente por meio de assinaturas digitais), e um
nó malicioso é capaz de guardar o pacote criptografado da jogada de algum
jogador para que possa retransmitir a jogada futuramente quantas vezes quiser
\citep{corman2006secure}.


Exemplo:
\begin{itemize}[label={}]
    \item Gênero: Simulação 2D.
    \item Contexto: $A$ efetua jogadas de movimentação nas quatro direções no plano, gerando pacotes assinados $P1$, $P2$, $P3$, $P4$.
    \item Trapaça: $M$ difunde cópias dos pacotes $P1$, $P2$, $P3$, $P4$ com propósito de mover $A$ para uma posição específica.
\end{itemize}

\subsection{Oponentes cegos}
Essa trapaça pode ocorrer em arquiteturas híbridas, quando o jogador malicioso envia suas 
jogadas somente ao um nó servidor, deixando os outros nós jogadores
com estados do jogo inconsistentes e portanto sendo necessário 
que o nó servidor mande atualizações para que os outros nós revertam
seus estados.

Exemplo 1:
\begin{itemize}[label={}]
    \item Gênero: Simulação.
    \item Trapaça: $M$ se movimenta no mundo sem informar $A$. 
    % Na perspectiva de $A$, 
    % $M$ estará parado, mas para o nó servidor que possui o estado global definitivo do 
    % jogo, $M$ estará se movimentando.
    Dessa forma, o nó servidor precisará conferir que o estado de jogo de $A$ está inconsistente e enviar um pacote para corrigir esse estado e por isso $A$ demora mais para receber a posição de $M$, diminuindo as chances para interagir com $M$.
\end{itemize}

Exemplo 2:
\begin{itemize}[label={}]
    \item Gênero: Jogo de cartas.
    \item Trapaça: $M$ põe cartas na mesa sem informar $A$ que espera indefinidamente a jogada de $M$ para começar seu turno.
\end{itemize}

\subsection{Revogação}
A trapaça por revogação
pode acontecer em protocolos que os jogadores anunciam uma assinatura das suas atualizações antes de transmiti-las. Essa trapaça ocorre 
quando um jogador anuncia sua jogada 
mas decide não envia-la após receber as jogadas dos adversários,
desfazendo a sua jogada.

% acontece em protocolos nos quais os nós
%criptografam suas jogadas e posteriormente revelam a chave (geralmente 
%após todos os jogadores transmitirem suas jogadas criptografadas).
% A trapaça ocorre na fase de revelamento das chaves, quando um peer mal-intencionado
%tem acesso às jogadas descriptografadas e decide não revelar sua chave,
% \citet{webb2007racs}.


Exemplo:
\begin{itemize}[label={}]
    \item Gênero: Jogo de perguntas e respostas.
    \item Contexto: $M$ escolha resposta 1; $A$, $B$ e $C$ escolhem a resposta 2.
    \item Trapaça: $M$, $A$, $B$ e $C$ divulgam suas respectivas jogadas criptografadas: 
    $J_M$, $J_A$, $J_B$ e $J_C$. Então $A$, $B$ e $C$ revelam suas respectivas chaves 
    $C_A$, $C_B$ e $C_C$, e $M$ decide não divulgar sua chave, pois percebeu 
    que todos escolheram uma resposta diferente, anulando sua jogada para não perder pontos.
\end{itemize}

\section{Nível de infraestrutura}
\label{sec:infraestrutura}

As trapaças a nível de infraestrutura consistem em monitorar ou 
fazer alterações no \textit{software} ou no hardware usados pelo jogo, atuando
como um \textit{proxy} entre a aplicação do jogador mal-intencionado e os 
outros nós, ou entre a aplicação e sua interface. Note que criptografar
os pacotes não é uma solução definitiva, pois não é possível garantir 
que a chave armazenada na aplicação não é acessível.

\subsection{Exposição de informação}
Assim como na trapaça de mesmo nome apresentada no nível de aplicação, 
o jogador consegue ler conteúdo não exposto ao usuário. Porém, o processo
para obter essas informações não envolve obter os dados diretamente da aplicação,
mas através de monitoramento de recursos externos ao \textit{software} do jogo.

Exemplo 1:
\begin{itemize}[label={}]
    \item Contexto: $M$ utiliza um computador como \textit{proxy} para os pacotes enviados 
    de outra máquina que roda o jogo.
    \item Trapaça: $M$ utiliza um \textit{\textit{software}} nesse computador que exibe um mapa do jogo com as posições dos 
    outros jogadores. 
\end{itemize}
Exemplo 2:
\begin{itemize}[label={}]
    \item Gênero: FPS.
    \item Contexto: $M$ altera o \textit{driver} de áudio e adiciona uma rotina que 
    exibe numa tela a posição estimada da origem de um som.
    \item Trapaça: $A$ anda, produzindo barulhos de passos e consequentemente
    $M$ descobre sua posição. 
\end{itemize}
Exemplo 3:
\begin{itemize}[label={}]
    \item Gênero: Jogo de ritmo.
    \item Contexto: $M$ altera o \textit{driver} do \textit{joystick} a fim de medir e exibir o 
    intervalo de tempo entre os pressionamentos dos botões.
    \item Trapaça: $M$ consegue se adaptar melhor ao ritmo do jogo. 
\end{itemize}

\subsection{Manipulação}
Do mesmo modo que a trapaça anterior, a trapaça por manipulação age sobre 
os intermediários utilizados pelo \textit{software} do jogo, contudo, ao invés de 
acessar os conteúdos, o jogador modifica, remove ou insere os dados 
transmitidos entre o jogo e esses intermediários.

Exemplo 1:
\begin{itemize}[label={}]
    \item Gênero: FPS.
    \item Contexto: $M$ utiliza um computador como \textit{proxy} para os pacotes enviados 
    de outra máquina que roda o jogo.
    \item Trapaça: $M$ altera os pacotes com o propósito de aumentar a precisão 
    dos seus disparos ao alvo.
\end{itemize}

Exemplo 2:
\begin{itemize}[label={}]
    \item Gênero: Jogo de ritmo.
    \item Contexto: $M$ insere uma rotina que simula o pressionamento
    de botões no \textit{driver} do \textit{joystick}.
    \item Trapaça: $M$ passa como entrada as notas musicais de determinada música
    de antemão para a rotina
    e como resultado toca a musica perfeitamente. 
\end{itemize}



